import requests
from bs4 import BeautifulSoup
import csv

url = "https://www.ndtv.com/top-stories#pfrom=home-ndtv_topstories"

response = requests.get(url)
if response.status_code == 200:
    soup = BeautifulSoup(response.text, "html.parser")
    headlines_container = soup.find("div", {"class": "lisingNews"})
    if headlines_container:
        news_data = []
        news_articles = headlines_container.find_all("div", {"class": "news_Itm-img"})
        for article in news_articles:
            soup = BeautifulSoup(str(article), "html.parser")
            anchor = soup.find('a')
            title = anchor.img['alt']
            link = anchor['href']
            print('======================')
            print(title,"title")
            print(link,"link")
            news_data.append([title, link])
        with open("ndtv_news_headlines.csv", "w", newline="") as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerows(news_data)